![输入图片说明](easy-log/src/main/resources/logo/EasyLog.png)

**Easy Log 一款轻量的日志脚手架**

>**gitee地址**：[https://gitee.com/liuhao3169/easy-log](https://gitee.com/liuhao3169/easy-log)
# 什么是Easy Log？

Easy Log是基于Spring Boot 2.6.7开发的一款轻量级的日志脚手架，实际的业务开发中，日志是必不可少的，引入Easy Log会为你的系统提供 **接口请求日志**、**错误日志**，并且可以基于编程的方式实现 **操作的记录日志**。

* 接口请求日志 
    * 方便开发环境中调试接口
    * 基于注解实现，可选择性的持久化接口请求日志
* 错误日志
    * 生产环境的系统错误信息会被持久化，方便排错
* 操作记录日志
    * 在需要的代码中调用Api即可实现操作日志
# 快速开始

## 引入pom坐标

* pom坐标
```java
<groupId>com.idea</groupId>
<artifactId>easy-log</artifactId>
<version>1.1.2.RELEASE</version>
```

## 初始化数据库

easy_log_api表

```java
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easy_log_api
-- ----------------------------
DROP TABLE IF EXISTS `easy_log_api`;
CREATE TABLE `easy_log_api`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `app_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务名',
  `server_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务器名',
  `app_port` int(11) DEFAULT NULL COMMENT '应用端口',
  `server_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务器IP地址',
  `env` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '环境',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求方式',
  `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求URI',
  `user_agent` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户代理',
  `visitor_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作IP地址',
  `method_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法类',
  `method_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法名',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '操作提交的数据',
  `time` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '执行时间',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '日志标题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

```
easy_log_error表
```sql
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easy_log_error
-- ----------------------------
DROP TABLE IF EXISTS `easy_log_error`;
CREATE TABLE `easy_log_error`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `app_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '应用名',
  `server_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务器名',
  `app_port` int(11) DEFAULT NULL COMMENT '应用端口',
  `server_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务器IP地址',
  `env` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '环境',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作方式',
  `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求URI',
  `user_agent` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户代理',
  `visitor_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作IP地址',
  `method_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法类',
  `method_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法名',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '操作提交的数据',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `stack_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '堆栈',
  `error_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '异常名',
  `error_message` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '异常信息',
  `error_line_number` int(11) DEFAULT NULL COMMENT '错误行数',
  `error_file_name` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '报错的Java文件名',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
```
easy_log_custom表
```java
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for easy_log_custom
-- ----------------------------
DROP TABLE IF EXISTS `easy_log_custom`;
CREATE TABLE `easy_log_custom`  (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `app_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务名',
  `server_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务器名',
  `app_port` int(11) DEFAULT NULL COMMENT '应用端口',
  `server_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '服务器IP地址',
  `env` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '环境',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作方式',
  `request_uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '请求URI',
  `visitor_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '操作IP地址',
  `method_class` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法类',
  `method_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '方法名',
  `user_agent` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '用户代理',
  `params` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '操作提交的数据',
  `create_user` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建者',
  `create_time` datetime(0) DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `log_level` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '日志级别',
  `log_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '日志业务id',
  `log_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '日志数据',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
```
## 在application.yml文件中配置easy-log

* 开启easy log，默认为关闭状态
```java
easy-log:
  enabled: true
```

## 在项目中实现IEasyLogService接口

* 该接口用于存储日志数据，每个方法的参数分别与上文中的初始化数据库表对应
```java
@Service
public class LogServiceImpl implements IEasyLogService{
    @Override
    public void saveApiLog(ApiLogModel apiLogModel) {
        //保存接口请求日志
        System.out.println("saveApiLog---->存入数据库"+apiLogModel);
    }
    @Override
    public void saveErrorLog(ErrorLogModel errorLogModel) {
        //保存错误日志
        System.out.println("saveErrorLog---->存入数据库"+errorLogModel);
    }
    @Override
    public void saveCustomLog(CustomLogModel customLogModel) {
        //保存自定义操作日志
        System.out.println("saveCustomLog---->存入数据库"+customLogModel);
    }
}
```



## 接口请求日志

**接口请求日志有两种：**

1. 控制台接口请求日志
2. 持久化接口请求日志
### 控制台接口请求日志

**介绍**

控制台接口请求日志，控制台上面打印的接口请求日志，这种日志不会持久化到数据库中，只是在控制台输出，方便在开发环境中调试接口。

**模式选择**

Easy Log对该日志提供了一些模式选择，用户可以在必要的情况下，通过选择模式来控制这种日志的输出信息量。

* 一共有四种模式
    * ALL 模式：全量模式，控制台输出请求接口地址、请求方法、请求头、请求参数、响应结果；
    * BASIC 模式：基本模式，控制台输出请求接口地址、请求方法、请求参数、响应结果；
    * SIMPLE 模式：简单模式，控制台输出请求地址、请求方法、响应结果；
    * CLOSE 模式：关闭模式，即停用该功能。
我们可以在application.yml文件中配置，来进行模式的切换，如下：

```yaml
easy-log:
  enabled: true
  console-log-mode: simple #控制台日志模式：简单模式
```


### 持久化接口请求日志

**介绍**

如果想记录某个接口的日志信息到数据库中，Easy Log提供了注解（该注解作用在方法上），用户可以选择性的将接口的日志信息持久化，注解如下：

```java
@ApiLog(value)
```
>该注解接收一个value值，一般用来描述接口的功能，但这并没有固定写法，用户可以自行定义规范
* 示例
```java
@PostMapping("/user")
@ApiLog("用户新增")
public R save(@RequestBody User user){
    //....
    //....
    return R.success("保存成功！");
}
```

## 错误日志

正常情况下错误日志并不是手动去记录的，而是通过捕获异常的方法去自动记录日志，所以Easy Log提供了一个内置的全局异常处理器RestExceptionTranslator类，用来处理系统中的一些异常，具体的逻辑可以看下源码，该类位于如下位置：

```java
com.idea.easy.log.translator.RestExceptionTranslator;
```

## 自定义操作日志

如果想要在系统中加入操作日志，我们可以使用如下Api：

```java
//info级别日志
EasyLogger.info("日志标识","日志信息");
//debug级别日志
EasyLogger.debug("日志标识","日志信息");
//warn级别日志
EasyLogger.warn("日志标识","日志信息");
//error级别日志
EasyLogger.error("日志标识","日志信息");
```
>日志标识：该参数没有固定的写法，一般建议为 模块名:操作名称，用户可以自行定义规则
* 示例
```java
@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private EasyLogger logger; //注入EasyLogger对象
    
    @PostMapping("/save")
    @ApiLog("用户新增")
    public R save(@RequestBody User user){
        //保存info级别日志
        logger.info("用户模块:新增操作","测试接口。。。。");
        return R.success("保存成功！");
    }
}

```



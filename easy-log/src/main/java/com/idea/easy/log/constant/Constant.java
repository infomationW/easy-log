package com.idea.easy.log.constant;

/**
 * @className: Constant
 * @description: 常量管理
 * @author: salad
 * @date: 2022/6/1
 **/
public interface Constant {

    /**
     * 默认的应用名
     */
    String EASY_LOG = "easy-log";

    /**
     * 默认的环境
     */
    String ENV = "dev";

    /**
     * 用户代理的请求头参数名
     */
    String USER_AGENT_HEADER = "user-agent";

    /**
     * 请求开始字样的日志打印
     */
    String REQUEST_START_LOG = "================== Request Start ==================\n";

    /**
     * 请求结束字样的日志打印
     */
    String REQUEST_END_LOG = "================== Request End ==================\n";

    /**
     * 响应开始字样的日志打印
     */
    String RESPONSE_START_LOG = "================== Response Start ==================\n";

    /**
     * 响应结束字样的日志打印
     */
    String RESPONSE_END_LOG = "===============  Response End  ================\n";

}

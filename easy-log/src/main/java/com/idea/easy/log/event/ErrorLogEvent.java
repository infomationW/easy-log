package com.idea.easy.log.event;

import com.idea.easy.log.model.ErrorLogModel;
import org.springframework.context.ApplicationEvent;

/**
 * @className: ErrorLogEvent
 * @description: 错误日志的事件
 * @author: salad
 * @date: 2022/6/9
 **/
public class ErrorLogEvent extends ApplicationEvent {

	public ErrorLogEvent(ErrorLogModel source) {
		super(source);
	}

}

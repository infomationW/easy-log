package com.idea.easy.log.props;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @className: ConsoleLogMode
 * @description: 控制台日志的模式枚举类
 * @author: salad
 * @date: 2022/6/9
 **/
@Getter
@AllArgsConstructor
public enum ConsoleLogMode {

    /**
     * 全量模式
     */
    ALL(0),

    /**
     * 基本模式
     */
    BASIC(1), //打印请求地址+参数 、响应数据

    /**
     * 简单模式
     */
    SIMPLE(2), //打印请求地址 、响应数据

    /**
     * 关闭模式
     */
    CLOSE(3);


    private final int mode;

    public boolean eq(ConsoleLogMode mode){
       return this.getMode() == mode.getMode();
    }

    public boolean gt(ConsoleLogMode mode){
       return this.getMode() > mode.getMode();
    }

    public boolean lt(ConsoleLogMode mode){
       return this.getMode() < mode.getMode();
    }

}

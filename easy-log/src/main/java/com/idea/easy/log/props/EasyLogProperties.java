package com.idea.easy.log.props;

import com.idea.easy.log.constant.Constant;
import com.idea.easy.log.utils.SpringUtil;
import com.idea.easy.log.utils.str.StrUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @className: EasyLogProperties
 * @description:
 * @author: salad
 * @date: 2022/6/2
 **/
@Data
@ConfigurationProperties(
        prefix = "easy-log"
)
public class EasyLogProperties {

    /**
     * 是否开启EasyLog，默认关闭
     */
    private boolean enabled = false;

    /**
     * 是否开启自带的全局异常处理器
     */
    private boolean errorProcessor = true;

    /**
     * 控制台日志的模式
     */
    private ConsoleLogMode consoleLogMode = ConsoleLogMode.ALL;

    /**
     * 是否开启控制台打印的logo,默认开启
     */
    private boolean banner = true;

    /**
     * 日志记录的应用名备用值
     */
    private String appName = Constant.EASY_LOG;

    /**
     * 日志记录的应用环境备用值
     */
    private String env = Constant.ENV;



}

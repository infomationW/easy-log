package com.idea.easy.log.provider;

import com.idea.easy.log.constant.Constant;
import com.idea.easy.log.model.LogCommonModel;
import com.idea.easy.log.props.EasyLogProperties;
import com.idea.easy.log.utils.*;
import com.idea.easy.log.utils.str.StrUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.Map;

/**
 * @className: EasyLogInfoUtil
 * @description: 获取并设置日志信息的工具类
 * @author: salad
 * @date: 2022/6/1
 **/
public class EasyLogInfoUtil {

    public static void appendRequestInfo(HttpServletRequest request, LogCommonModel model) {
        model.setVisitorIp(WebUtil.getIP(request));
        model.setUserAgent(request.getHeader(Constant.USER_AGENT_HEADER));
        model.setRequestUri(WebUtil.getPath(request.getRequestURI()));
        model.setRequestMethod(request.getMethod());
        model.setParams(WebUtil.getRequestParamToStr(request));
    }

    public static void appendServerInfo(LogCommonModel model, EasyLogServerInfoProvider provider, EasyLogProperties properties) {
        model.setAppName(StrUtils.isBlank(SpringUtil.getAppName())  ? properties.getAppName() : SpringUtil.getAppName());
        model.setServerName(INetUtil.getHostName());
        model.setServerIp(provider.getIp());
        model.setAppPort(provider.getPort());
        model.setEnv(StrUtils.isBlank(SpringUtil.getEnv()) ? properties.getEnv() : SpringUtil.getEnv());
        model.setCreateTime(DateUtil.now());
    }

}

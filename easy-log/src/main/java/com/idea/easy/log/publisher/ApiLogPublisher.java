package com.idea.easy.log.publisher;

import com.idea.easy.log.provider.EasyLogInfoUtil;
import com.idea.easy.log.model.ApiLogModel;
import com.idea.easy.log.utils.SpringUtil;
import com.idea.easy.log.utils.WebUtil;
import org.springframework.context.ApplicationEvent;

import javax.servlet.http.HttpServletRequest;

/**
 * @className: ApiLogPublisher
 * @description: ApiLog事件发布者
 * @author: salad
 * @date: 2022/6/3
 **/
public class ApiLogPublisher extends LogPublisher{

    public ApiLogPublisher(ApplicationEvent event) {
        setEvent(event);
    }

    public static ILogPublisher event(ApplicationEvent event){
        return new ApiLogPublisher(event);
    }

    @Override
    public void publish() {
        HttpServletRequest request = WebUtil.getRequest();
        ApplicationEvent event = getEvent();
        EasyLogInfoUtil.appendRequestInfo(request,(ApiLogModel)event.getSource());
        SpringUtil.publishEvent(event);
    }

}

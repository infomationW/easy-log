package com.idea.easy.log.service;

import com.idea.easy.log.model.ApiLogModel;
import com.idea.easy.log.model.CustomLogModel;
import com.idea.easy.log.model.ErrorLogModel;

/**
 * @className: IEasyLogService
 * @description: 提供持久化日志方法的接口，用户需要实现这个接口
 * @author: salad
 * @date: 2022/6/4
 **/
public interface IEasyLogService {

  void saveApiLog(ApiLogModel apiLogModel);


  void saveErrorLog(ErrorLogModel errorLogModel);


  void saveCustomLog(CustomLogModel customLogModel);

}
